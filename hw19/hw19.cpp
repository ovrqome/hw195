﻿
#include <iostream>
#include <string>
#include <cmath>


using namespace std;



class Animal
{
public:
	virtual void voice()
	{
		cout << "Class animals";
	}
};

class Dog : public Animal
{
public:
	virtual void voice() override
	{
		cout << "Woof! \n";
	}
};

class Cat : public Animal
{
public:
	virtual void voice() override
	{
		cout << "Meow \n";
	}
};

class Human : public Animal
{
public:
	virtual void voice() override
	{
		cout << "I human \n";
	}
};

int main()
{
	Animal* animals[3];
	animals[0] = new Dog();
	animals[1] = new Cat();
	animals[2] = new Human();
	

	for (Animal* a : animals)
		a->voice();
}